package main

import (
	_ "chk/docs"
	"chk/internal/handler"
	"chk/internal/repository"
	"chk/internal/usecase"
	"chk/pkg/model"
	"chk/pkg/mysql"
	"flag"
	"fmt"
	swagger "github.com/arsmn/fiber-swagger/v2"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/compress"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/gofiber/fiber/v2/middleware/requestid"
	"github.com/spf13/viper"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

const SwaggerPath = "/swagger"
const HealthCheckPath = "/healthcheck"

var env string

// @title CHK APIs
// @version 1.0
func main() {
	flag.StringVar(&env, "env", "development", "Config env: development, production")
	flag.Parse()

	viper.SetConfigFile(`env/development.yml`)
	if err := viper.ReadInConfig(); err != nil {
		panic(err)
	}
	viper.AutomaticEnv()
	db, err := mysql.Init()
	if err != nil {
		log.Fatal(err)
	}
	err = db.Set("gorm:table_options", "ENGINE=MyISAM").AutoMigrate(&model.OhlcData{})
	if err != nil {
		log.Fatal(err)
	}
	app := newApp()
	// Middleware for /api/v1
	apiV1 := app.Group("/api/v1", func(c *fiber.Ctx) error {
		c.Set("version", "v1")
		return c.Next()
	})

	repo := repository.NewOhlcDataRepoImpl(db)
	uc := usecase.NewOhlcDataUseCaseImpl(repo)
	handler.NewOhlcDataHandlerHandler(apiV1, uc)

	go func() {
		if err := app.Listen(fmt.Sprintf(":%d", viper.GetInt(`SERVER_HTTP_PORT`))); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %v\n", err)
		}
	}()

	// graceful shutdown
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Print("Shutting down server...")
	if err := app.Shutdown(); err != nil {
		log.Fatalf("Server forced to shutdown: %v", err)
	}
	defer func() {
	}()
	log.Print("Server exiting")
}

func LogMiddleware() fiber.Handler {
	return logger.New(logger.Config{
		Format:     "{\"username\":\"${locals:username}\",\"requestid\":\"${locals:requestid}\",\"status\":\"${status}\",\"method\":\"${method}\",\"path\":\"${path}\",\"queryParams\":\"${queryParams}\",\"latency\":\"${latency}\",\"error\":\"${error}\",\"ts\":\"${time}\"}\n",
		Output:     os.Stdout,
	})
}

func RecoverHandler() fiber.Handler {
	return recover.New()
}

func CompressHandler() fiber.Handler {
	return compress.New()
}

func CorsHandler() fiber.Handler {
	return cors.New()
}

func RequestIdHandler() fiber.Handler {
	return requestid.New()
}

func newApp() *fiber.App {
	app := fiber.New()
	app.Use(RecoverHandler())
	app.Use(RequestIdHandler())
	app.Use(LogMiddleware())
	app.Use(CorsHandler())
	app.Use(CompressHandler())

	// Health check
	app.Get(HealthCheckPath, func(c *fiber.Ctx) error {
		return c.SendString("UP")
	})
	app.Get(fmt.Sprintf("%s/*", SwaggerPath), swagger.New())
	return app
}



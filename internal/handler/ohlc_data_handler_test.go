package handler

import (
	"bytes"
	"chk/internal/usecase/mocks"
	"chk/pkg/model"
	"encoding/json"
	"github.com/gocarina/gocsv"
	"github.com/gofiber/fiber/v2"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"mime/multipart"
	"net/http/httptest"
	"testing"
)

func TestNewOhlcDataHandlerHandler_importOhlcData_success(t *testing.T) {
	csvRows := []*model.OhlcData{
		{
			Unix:   1,
			Symbol: "ABCD",
			Open:   1,
			High:   2,
			Low:    3,
			Close:  4,
		},
		{
			Unix:   2,
			Symbol: "ABCD",
			Open:   5,
			High:   6,
			Low:    7,
			Close:  8,
		},
	}
	app := fiber.New()
	uc := &mocks.OhlcDataUseCase{}
	uc.On("SaveMany", csvRows).Return(nil)
	NewOhlcDataHandlerHandler(app, uc)
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	fileWriter, _ := w.CreateFormFile("file", "filename")

	bytes, _ := gocsv.MarshalBytes(&csvRows)
	_, _ = fileWriter.Write(bytes)
	w.Close()
	request := httptest.NewRequest("POST", "/ohlc-data", &b)
	request.Header.Set(fiber.HeaderContentType, w.FormDataContentType())
	res, err := app.Test(request)
	assert.Nil(t, err)
	assert.Equal(t, 200, res.StatusCode)
}

func TestNewOhlcDataHandlerHandler_importOhlcData_fail_validation_symbol(t *testing.T) {
	csvRows := []*model.OhlcData{
		{
			Unix:   1,
			Symbol: "",
			Open:   1,
			High:   2,
			Low:    3,
			Close:  4,
		},
		{
			Unix:   2,
			Symbol: "ABCD",
			Open:   5,
			High:   6,
			Low:    7,
			Close:  8,
		},
	}
	app := fiber.New()
	uc := &mocks.OhlcDataUseCase{}
	uc.On("SaveMany", csvRows).Return(nil)
	NewOhlcDataHandlerHandler(app, uc)
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	fileWriter, _ := w.CreateFormFile("file", "filename")

	bytes, _ := gocsv.MarshalBytes(&csvRows)
	_, _ = fileWriter.Write(bytes)
	w.Close()
	request := httptest.NewRequest("POST", "/ohlc-data", &b)
	request.Header.Set(fiber.HeaderContentType, w.FormDataContentType())
	res, err := app.Test(request)
	assert.Nil(t, err)
	assert.Equal(t, 400, res.StatusCode)
}

func TestNewOhlcDataHandlerHandler_importOhlcData_fail_validation_open(t *testing.T) {
	csvRows := []*model.OhlcData{
		{
			Unix:   1,
			Symbol: "ABCD",
			Open:   0,
			High:   2,
			Low:    3,
			Close:  4,
		},
		{
			Unix:   2,
			Symbol: "ABCD",
			Open:   5,
			High:   6,
			Low:    7,
			Close:  8,
		},
	}
	app := fiber.New()
	uc := &mocks.OhlcDataUseCase{}
	uc.On("SaveMany", csvRows).Return(nil)
	NewOhlcDataHandlerHandler(app, uc)
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	fileWriter, _ := w.CreateFormFile("file", "filename")

	bytes, _ := gocsv.MarshalBytes(&csvRows)
	_, _ = fileWriter.Write(bytes)
	w.Close()
	request := httptest.NewRequest("POST", "/ohlc-data", &b)
	request.Header.Set(fiber.HeaderContentType, w.FormDataContentType())
	res, err := app.Test(request)
	assert.Nil(t, err)
	assert.Equal(t, 400, res.StatusCode)
}

func TestNewOhlcDataHandlerHandler_importOhlcData_fail_validation_high(t *testing.T) {
	csvRows := []*model.OhlcData{
		{
			Unix:   1,
			Symbol: "ABCD",
			Open:   1,
			High:   0,
			Low:    3,
			Close:  4,
		},
		{
			Unix:   2,
			Symbol: "ABCD",
			Open:   5,
			High:   6,
			Low:    7,
			Close:  8,
		},
	}
	app := fiber.New()
	uc := &mocks.OhlcDataUseCase{}
	uc.On("SaveMany", csvRows).Return(nil)
	NewOhlcDataHandlerHandler(app, uc)
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	fileWriter, _ := w.CreateFormFile("file", "filename")

	bytes, _ := gocsv.MarshalBytes(&csvRows)
	_, _ = fileWriter.Write(bytes)
	w.Close()
	request := httptest.NewRequest("POST", "/ohlc-data", &b)
	request.Header.Set(fiber.HeaderContentType, w.FormDataContentType())
	res, err := app.Test(request)
	assert.Nil(t, err)
	assert.Equal(t, 400, res.StatusCode)
}

func TestNewOhlcDataHandlerHandler_importOhlcData_fail_validation_low(t *testing.T) {
	csvRows := []*model.OhlcData{
		{
			Unix:   1,
			Symbol: "ABCD",
			Open:   1,
			High:   2,
			Low:    0,
			Close:  4,
		},
		{
			Unix:   2,
			Symbol: "ABCD",
			Open:   5,
			High:   6,
			Low:    7,
			Close:  8,
		},
	}
	app := fiber.New()
	uc := &mocks.OhlcDataUseCase{}
	uc.On("SaveMany", csvRows).Return(nil)
	NewOhlcDataHandlerHandler(app, uc)
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	fileWriter, _ := w.CreateFormFile("file", "filename")

	bytes, _ := gocsv.MarshalBytes(&csvRows)
	_, _ = fileWriter.Write(bytes)
	w.Close()
	request := httptest.NewRequest("POST", "/ohlc-data", &b)
	request.Header.Set(fiber.HeaderContentType, w.FormDataContentType())
	res, err := app.Test(request)
	assert.Nil(t, err)
	assert.Equal(t, 400, res.StatusCode)
}

func TestNewOhlcDataHandlerHandler_importOhlcData_fail_validation_close(t *testing.T) {
	csvRows := []*model.OhlcData{
		{
			Unix:   1,
			Symbol: "ABCD",
			Open:   1,
			High:   2,
			Low:    2,
			Close:  0,
		},
		{
			Unix:   2,
			Symbol: "ABCD",
			Open:   5,
			High:   6,
			Low:    7,
			Close:  8,
		},
	}
	app := fiber.New()
	uc := &mocks.OhlcDataUseCase{}
	uc.On("SaveMany", csvRows).Return(nil)
	NewOhlcDataHandlerHandler(app, uc)
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	fileWriter, _ := w.CreateFormFile("file", "filename")

	bytes, _ := gocsv.MarshalBytes(&csvRows)
	_, _ = fileWriter.Write(bytes)
	w.Close()
	request := httptest.NewRequest("POST", "/ohlc-data", &b)
	request.Header.Set(fiber.HeaderContentType, w.FormDataContentType())
	res, err := app.Test(request)
	assert.Nil(t, err)
	assert.Equal(t, 400, res.StatusCode)
}

func TestNewOhlcDataHandlerHandler_getOhlcData(t *testing.T) {
	query := &model.OhlcDataQuery{
		UnixFrom: 1,
		UnixTo:   2,
		Symbol:   "ABCD",
		LastId:   3,
		Limit:    4,
	}
	app := fiber.New()
	uc := &mocks.OhlcDataUseCase{}
	uc.On("Query", query).Return(&model.OhlcDataResponse{
		OhlcData: []*model.OhlcData{
			{
				Id:        1,
				Unix:      1,
				Symbol:    "ABCD",
				Open:      1,
				High:      2,
				Low:       3,
				Close:     4,
			},
			{
				Id:        2,
				Unix:      2,
				Symbol:    "ABCD",
				Open:      5,
				High:      6,
				Low:       7,
				Close:     8,
			}},
		Total: 2,
	}, nil)
	NewOhlcDataHandlerHandler(app, uc)
	res, err := app.Test(httptest.NewRequest("GET", "/ohlc-data?unix_from=1&unix_to=2&symbol=ABCD&last_id=3&limit=4", nil))
	assert.Nil(t, err)
	body, err := ioutil.ReadAll(res.Body)
	assert.Nil(t, err)
	result := &model.OhlcDataResponse{}
	err = json.Unmarshal(body, result)
	assert.Nil(t, err)
	assert.Equal(t, uint(1), result.OhlcData[0].Id)
	assert.Equal(t, int64(1), result.OhlcData[0].Unix)
	assert.Equal(t, "ABCD", result.OhlcData[0].Symbol)
	assert.Equal(t, float64(1), result.OhlcData[0].Open)
	assert.Equal(t, float64(2), result.OhlcData[0].High)
	assert.Equal(t, float64(3), result.OhlcData[0].Low)
	assert.Equal(t, float64(4), result.OhlcData[0].Close)
	assert.Equal(t, int64(2), result.Total)
}

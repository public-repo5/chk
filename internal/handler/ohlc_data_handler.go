package handler

import (
	"chk/internal/usecase"
	"chk/pkg/model"
	"encoding/csv"
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/gocarina/gocsv"
	"github.com/gofiber/fiber/v2"
	"io"
	"io/ioutil"
)

type ohlcDataHandler struct {
	validator *validator.Validate
	uc        usecase.OhlcDataUseCase
}

func NewOhlcDataHandlerHandler(router fiber.Router, uc usecase.OhlcDataUseCase) {
	handler := &ohlcDataHandler{
		validator: validator.New(),
		uc:        uc,
	}
	group := router.Group("/ohlc-data")
	group.Post("/", handler.importOhlcData)
	group.Get("/", handler.getOhlcData)
}

// importOhlcData godoc
// @Summary Import OHLC Data
// @Description Import OHLC Data
// @Tags OhlcData
// @Accept  multipart/form-data
// @Produce  json
// @Param file formData file true "File to be imported"
// @Success 200
// @Failure 400
// @Failure 500
// @Router /api/v1/ohlc-data [post]
func (h ohlcDataHandler) importOhlcData(ctx *fiber.Ctx) error {
	data, err := parseCSV(ctx)
	if err != nil {
		return err
	}
	for idx, row := range data {
		if err = h.validator.Struct(row); err != nil {
			return fiber.NewError(fiber.StatusBadRequest, fmt.Sprintf("%s at row %d", err.Error(), idx+1))
		}
	}
	if err = h.uc.SaveMany(data); err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, err.Error())
	}
	return nil
}

// getOhlcData godoc
// @Summary List OHLC Data
// @Description List OHLC Data
// @Tags OhlcData
// @Accept  json
// @Produce  json
// @Param unix_from query int true "Unix From"
// @Param unix_to query int true "Unix To"
// @Param symbol query string true "Symbol"
// @Param last_id query int false "Last Id"
// @Param limit query int false "Limit"
// @Success 200 {object} model.OhlcDataResponse
// @Failure 400
// @Failure 500
// @Router /api/v1/ohlc-data [get]
func (h ohlcDataHandler) getOhlcData(ctx *fiber.Ctx) error {
	query := &model.OhlcDataQuery{}
	if err := ctx.QueryParser(query); err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, err.Error())
	}
	result, err := h.uc.Query(query)
	if err != nil {
		return fiber.NewError(fiber.StatusInternalServerError, err.Error())
	}
	return ctx.JSON(result)
}

func parseCSV(ctx *fiber.Ctx) ([]*model.OhlcData, error) {
	fileHeader, err := ctx.FormFile("file")
	if err != nil {
		return nil, fiber.NewError(fiber.StatusBadRequest, err.Error())
	}
	file, err := fileHeader.Open()
	if err != nil {
		panic(err)
	}

	gocsv.SetCSVReader(func(in io.Reader) gocsv.CSVReader {
		r := csv.NewReader(in)
		r.Comma = ','
		return r
	})

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		panic(err)
	}
	var data []*model.OhlcData
	if err = gocsv.UnmarshalBytes(fileBytes, &data); err != nil {
		panic(err)
	}
	if data == nil || len(data) < 1 {
		return nil, fiber.NewError(fiber.StatusBadRequest, "file is empty")
	}
	return data, nil
}

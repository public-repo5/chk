package usecase

import (
	"chk/internal/repository"
	"chk/pkg/model"
)

const MaxLimit = 100

type OhlcDataUseCase interface {
	SaveMany(ohlcData []*model.OhlcData) error
	Query(query *model.OhlcDataQuery) (*model.OhlcDataResponse, error)
}

type ohlcDataUseCaseImpl struct {
	repo repository.OhlcDataRepo
}

func (o ohlcDataUseCaseImpl) SaveMany(ohlcData []*model.OhlcData) error {
	return o.repo.Database().CreateInBatches(ohlcData, len(ohlcData)).Error
}

func (o ohlcDataUseCaseImpl) Query(query *model.OhlcDataQuery) (*model.OhlcDataResponse, error) {
	limit := MaxLimit
	if query.Limit < limit {
		limit = query.Limit
	}
	var result []*model.OhlcData
	tx := o.repo.Database().Limit(limit).Where("symbol = ? AND id > ? AND unix >= ? AND unix <= ?",
		query.Symbol, query.LastId, query.UnixFrom, query.UnixTo).Order("unix desc, id desc").Find(&result)
	if tx.Error != nil {
		return nil, tx.Error
	}
	return &model.OhlcDataResponse{OhlcData: result, Total: tx.RowsAffected}, nil
}

func NewOhlcDataUseCaseImpl(repo repository.OhlcDataRepo) *ohlcDataUseCaseImpl {
	return &ohlcDataUseCaseImpl{repo: repo}
}

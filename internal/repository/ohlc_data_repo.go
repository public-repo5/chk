package repository

import (
	"gorm.io/gorm"
)

type OhlcDataRepo interface {
	Database() *gorm.DB
}

type ohlcDataRepoImpl struct {
	DB *gorm.DB
}

func (o ohlcDataRepoImpl) Database() *gorm.DB {
	return o.DB
}

func NewOhlcDataRepoImpl(DB *gorm.DB) *ohlcDataRepoImpl {
	return &ohlcDataRepoImpl{DB: DB}
}


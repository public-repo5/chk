FROM golang:1.18-alpine AS builder
WORKDIR /go/src/app

RUN apk update && apk upgrade && \
  apk --update add git gcc make libc-dev openssh linux-headers

COPY ./go.mod ./go.sum ./
RUN go mod download

COPY . .

RUN go mod tidy
RUN go build -o build/main cmd/*.go

FROM alpine as release
WORKDIR /app

RUN apk update && apk upgrade && \
  apk --update --no-cache add tzdata ca-certificates linux-headers

COPY --from=builder /go/src/app/build/main main
COPY --from=builder /go/src/app/env env

ENTRYPOINT [ "./main", "-env=production" ]

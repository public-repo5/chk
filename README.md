
# Run project
### requirement
- go version go1.16 darwin/amd64 or latest

### Test
```
go test ./...
```

### Linter and check syntax code
```
golangci-lint run ./...
```

### Swagger UI
- [swagger format document](https://swaggo.github.io/swaggo.io/declarative_comments_format/)
```
# install swag cmd for the first time
go get -u github.com/swaggo/swag/cmd/swag

# init swag doc
swag init -g cmd/main.go

# browser to
http://127.0.0.1:8080/swagger/index.html

```
### Run with docker
- In env/development.yml, update mysql configuration
- Run `docker build -t chk .`
- Run `docker run -p8080:8080 chk`
- Browser to http://127.0.0.1:8080/swagger/index.html

### Test with curl
- Upload csv file
`curl -X POST "http://127.0.0.1:8080/api/v1/ohlc-data" -H "accept: application/json" -H "Content-Type: multipart/form-data" -F "file=@test.csv;type=text/csv"`
- Query data
`curl -X GET "http://127.0.0.1:8080/api/v1/ohlc-data?unix_from=1&unix_to=1644719470000&symbol=BTCUSDT&last_id=0&limit=3" -H "accept: application/json"`
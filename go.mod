module chk

go 1.16

require (
	github.com/arsmn/fiber-swagger/v2 v2.15.0
	github.com/go-playground/validator/v10 v10.11.0
	github.com/gocarina/gocsv v0.0.0-20220531201732-5f969b02b902
	github.com/gofiber/fiber/v2 v2.29.0
	github.com/pkg/errors v0.9.1
	github.com/spf13/viper v1.12.0
	github.com/stretchr/testify v1.7.1
	github.com/swaggo/swag v1.7.0
	gorm.io/driver/mysql v1.3.4
	gorm.io/gorm v1.23.6
)

package model

import "time"

type OhlcData struct {
	Id        uint      `gorm:"column:id;primaryKey" csv:"-" json:"id"`
	CreatedAt time.Time `json:"-" gorm:"date_created;autoCreateTime" csv:"-"`
	Unix      int64     `gorm:"unix" csv:"UNIX" json:"unix" validate:"required"`
	Symbol    string    `gorm:"index:idx_symbol;length:10;column:symbol;not null" csv:"SYMBOL" json:"symbol" validate:"required"`
	Open      float64   `gorm:"open" csv:"OPEN" json:"open" validate:"required"`
	High      float64   `gorm:"high" csv:"HIGH" json:"high" validate:"required"`
	Low       float64   `gorm:"low" csv:"LOW" json:"low" validate:"required"`
	Close     float64   `gorm:"close" csv:"CLOSE" json:"close" validate:"required"`
}

type OhlcDataQuery struct {
	UnixFrom int64  `query:"unix_from"`
	UnixTo   int64  `query:"unix_to"`
	Symbol   string `query:"symbol"`
	LastId   uint   `query:"last_id"`
	Limit    int    `query:"limit"`
}

type OhlcDataResponse struct {
	OhlcData []*OhlcData `json:"ohlc_data"`
	Total    int64       `json:"total"`
}

package mysql

import (
	"fmt"
	"net/url"
	"sync"

	"github.com/pkg/errors"
	"github.com/spf13/viper"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var (
	lock     sync.RWMutex
	instance *gorm.DB
)

func Init() (*gorm.DB, error) {
	lock.Lock()
	defer lock.Unlock()
	if instance == nil {
		db, err := connectDb()
		if err != nil {
			return nil, err
		}
		instance = db
	}
	return instance, nil
}

func connectDb() (*gorm.DB, error) {
	h := viper.GetString(`MYSQL_HOST`)
	dbName := viper.GetString(`MYSQL_DATABASE_NAME`)
	un := viper.GetString(`MYSQL_USER`)
	pw := viper.GetString(`MYSQL_PASSWORD`)
	p := viper.GetString(`MYSQL_PORT`)
	maxPoolSize := viper.GetInt(`MYSQL_MAX_POOL_SIZE`)

	connection := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", un, pw, h, p, dbName)
	val := url.Values{}
	val.Add("charset", "utf8mb4,utf8")
	val.Add("collation", "utf8mb4_unicode_ci")
	val.Add("parseTime", "True")
	val.Add("loc", "Local")

	dsn := fmt.Sprintf("%s?%s", connection, val.Encode())
	gormConfig := gorm.Config{}

	connect, err := gorm.Open(mysql.New(mysql.Config{
		DSN: dsn,
	}), &gormConfig)

	if err != nil {
		return nil, errors.Wrap(err, "gorm.Open")
	}

	sqlDB, err := connect.DB()

	if err != nil {
		return nil, errors.Wrap(err, "gorm.Open")
	}

	sqlDB.SetMaxOpenConns(20)
	sqlDB.SetMaxIdleConns(maxPoolSize)
	return connect, nil
}
